ASM=nasm
ASMFLAGS=-felf64
LD = ld

main: main.o dict.o lib.o
	$(LD) -o $@ $^


main.o: main.asm colon.inc lib.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

.PHONY: clean

clean:
	rm -rf *.o main

