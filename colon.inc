%define node_pointer 0

%macro colon 2 
    %2:
    dq node_pointer
    db %1, 0
    %define node_pointer %2
%endmacro
