%include "lib.inc"
%include "colon.inc"
%include "words.inc"

extern find_word

%assign MAX_SIZE 256  ;255 + terminator
%assign QWORD_SIZE 8

section .rodata 
invite_message: db "Hello! Try to find something.", 10, 0
not_found_message: db "Sorry! This value doesn't exists.", 10, 0
length_size_message: db "Sorry! Max size of string is 255.", 10, 0

section .bss
user_input: resb MAX_SIZE

global _start

section .text

print_error:
    push rdi
	call string_length
    pop rdi
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 2
	syscall
    ret

_start:
    mov rdi, invite_message
    call print_string

    mov rdi, user_input
    mov rsi, MAX_SIZE
    call read_string
    test rax, rax
    jz .length_size_error
    
    mov rdi, rax
    mov rsi, first_word
    call find_word
    test rax, rax
    jz .not_found 

    mov rdi, rax
    add rdi, QWORD_SIZE   
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call exit 

    .length_size_error:
        mov rdi, length_size_message
        call print_error
        call exit
    .not_found:
        mov rdi, not_found_message
        call print_error
        call exit
