%include "lib.inc"

global find_word

%assign QWORD_SIZE 8

section .text
;rdi - user string pointer
;rsi - dictionary start pointer

find_word:
    .loop:
        test rsi, rsi
        jz .not_found      ; if out of dict rsi == 0
        
        push rdi       ; try to compare strings
        push rsi
        add rsi, QWORD_SIZE     ; rsi - value marker
        call string_equals
        pop rsi
        pop rdi

        cmp rax, 1
        jz .found 

        mov rsi, [rsi]
        jmp .loop
    
    .found:
        mov rax, rsi
        ret
    .not_found:
        xor rax, rax
        ret
